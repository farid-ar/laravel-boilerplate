<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideshowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('slideshows')) {
            Schema::create('slideshows', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->nullable();
                $table->longText('legend')->nullable();
                $table->string('image_desktop')->nullable();
                $table->string('image_mobile')->nullable();
                $table->string('section')->nullable();
                $table->string('link')->nullable();
                $table->enum('target', array('_self', '_blank'))->nullable();
                $table->integer('order')->nullable()->unsigned();
                $table->integer('published')->nullable()->unsigned();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slideshows');
    }
}
