<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('news_posts')) {
            Schema::create('news_posts', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->nullable();
                $table->string('slug')->nullable();
                $table->text('content')->nullable();
                $table->string('image_desktop')->nullable();
                $table->string('image_mobile')->nullable();
                $table->string('pdf')->nullable();
                $table->string('url')->nullable();
                $table->string('youtube')->nullable();
                $table->integer('published')->nullable()->unsigned();
                $table->integer('featured')->nullable()->unsigned();
                $table->integer('category_id')->nullable()->unsigned();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_posts');
    }
}
