module.exports = {
  purge: [
  './resources/views/**/*.php',
  './resources/js/**/*.vue',
  './resources/js/**/*.svelte',
  ],
  theme: {
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      xxl: '1440px',
    },
    fontFamily: {
      sans: ['system-ui', '-apple-system', 'BlinkMacSystemFont', "Segoe UI", 'Roboto', "Helvetica Neue", 'Arial', "Noto Sans", 'sans-serif', "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"
      ],
      body: ['sans'],
      h1: ['sans'],
      h2: ['sans'],
      h3: ['sans'],
      strong: ['sans'],
    },
    borderWidth: {
      default: '1px',
      '0': '0',
      '2': '2px',
      '4': '4px',
    },
    aspectRatio: {
            none: 0,
            square: [1, 1],
            "16/9": [16, 9],
            "4/3": [4, 3],
            "21/9": [21, 9]
          },
    extend: {
      colors: {
        'gray-100': 'rgba(248, 249, 250, 1)', 
        'gray-200': 'rgba(233, 236, 239, 1)', 
        'gray-300': 'rgba(233, 236, 239, 1)', 
        'gray-400': 'rgba(206, 212, 218, 1)', 
        'gray-500': 'rgba(173, 181, 189, 1)', 
        'gray-600': 'rgba(108, 117, 125, 1)', 
        'gray-700': 'rgba(73, 80, 87, 1)', 
        'gray-800': 'rgba(52, 58, 64, 1)',
        'gray-900': 'rgba(33, 37, 41, 1)'
      },
      spacing: {
        '96': '24rem',
        '128': '32rem',
      },
      width:{
        'w-7' : '1.75rem',
      },
      height: {
        'h-screen' : 'calc(100vh - 100px)',
      }
    }
  },
  corePlugins: {},
  variants: {
    backgroundColor: ['responsive', 'hover', 'focus', 'group-hover', 'focus-within', 'odd', 'even', 'active'],
    textColor: ['responsive', 'hover', 'focus', 'group-hover', 'focus-within', 'active'],
    zIndex: ['responsive', 'focus'],
    aspectRatio: ['responsive']
  },
  plugins: [
    require("tailwindcss-responsive-embed"),
    require("tailwindcss-aspect-ratio"),
  ],
}
