<?php

namespace App\Observers;

use App\Models\ContactForm;
use App\Models\Setting;
use App\Notifications\NewContactForm;
use Illuminate\Support\Facades\Notification;

class ContactFormObserver
{
  //use Queueable;
  
    public function created(ContactForm $model)
    {

        $mailto = Setting::where('key', 'email_contacto')->first();
        try {
            Notification::route('mail', $mailto->value)->notify(new NewContactForm($model));
            Notification::route('mail', 'erikarubaja@hotmail.com')->notify(new NewContactForm($model));
            \Log::info('email de contacto enviado a ' . $mailto->value);
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
