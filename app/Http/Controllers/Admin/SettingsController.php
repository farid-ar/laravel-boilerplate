<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySettingRequest;
use App\Http\Requests\StoreSettingRequest;
use App\Http\Requests\UpdateSettingRequest;
use App\Models\Setting;
use Yajra\DataTables\Facades\DataTables;

class SettingsController extends Controller
{

    public function index()
    {
        //abort_unless(\Gate::allows('setting_access'), 403);

        $settings = Setting::all();

        return view('admin.settings.index', compact('settings'));
    }

    public function create()
    {
        //abort_unless(\Gate::allows('setting_create'), 403);

        return view('admin.settings.create');
    }

    public function store(StoreSettingRequest $request)
    {
        //abort_unless(\Gate::allows('setting_create'), 403);
        $setting = Setting::create($request->all());

        return redirect()->route('admin.settings.index');
    }

    public function edit(Setting $setting)
    {
        //abort_unless(\Gate::allows('setting_edit'), 403);
        return view('admin.settings.edit', compact('setting'));
    }

    public function update(UpdateSettingRequest $request, Setting $setting)
    {
        //abort_unless(\Gate::allows('setting_edit'), 403);
        $setting->update($request->all());

        return redirect()->route('admin.settings.index');
    }

    public function show(Setting $setting)
    {
        //abort_unless(\Gate::allows('setting_show'), 403);

        return view('admin.settings.show', compact('setting'));
    }

    public function destroy(Setting $setting)
    {
        //abort_unless(\Gate::allows('setting_delete'), 403);

        $setting->delete();

        return back();
    }

    public function massDestroy(MassDestroySettingRequest $request)
    {
        setting::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
