<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\DB;
use SEO;

class FrontBaseController extends Controller
{

    public function __construct()
    {
        $logo = \App\Models\Brand::
            where('slug', 'priux')
            ->first();
        $brands = \App\Models\Brand::
            where('slug', '<>', 'priux')
            ->orderBy('order')
            ->get();

        $browser = new Agent();
        $config_vars = \App\Models\Setting::
            all()->pluck('value', 'key');

        SEO::setTitle(config('app.name'));
        SEO::opengraph()->setUrl(url()->current());
        SEO::setCanonical(url()->current());
        SEO::opengraph()->addProperty('type', 'website');
        SEO::twitter()->setSite('');

        \View::share('logo', $logo);
        \View::share('brands', $brands);
        \View::share('browser', $browser);
        \View::share('config_vars', $config_vars);
    }
}
