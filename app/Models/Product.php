<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Product extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'products';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'slug',
        'description',
        'category_id',
        'order',
        'published',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static function boot()
    {
        parent::boot();

        Product::observe(new \App\Observers\SetSlugObserver);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(275)->height(275);
        $this->addMediaConversion('medium')->width(768)->height(768);
        $this->addMediaConversion('large')->width(1024)->height(1024);
        $this->addMediaConversion('xlarge')->width(1280)->height(1280);
        $this->addMediaConversion('2xlarge')->width(1600)->height(1600);
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function getLogoAttribute()
    {
        $file = $this->getMedia('logo')->last();
        if ($file) {
            $file->url = $file->getUrl();
        }

        return $file;
    }

    public function getPhotoAttribute()
    {
        $file = $this->getMedia('photo')->last();
        if ($file) {
            $file->url = $file->getUrl();
        }

        return $file;
    }

    public function getGalleryAttribute()
    {
        $files = $this->getMedia('gallery');

        $files->each(function ($item) {
            $item->url = $item->getUrl();
        });

        return $files;
    }

    public function getdatasheetAttribute()
    {
        return $this->getMedia('datasheet')->last();
    }

    public function setPublishedAttribute($input)
    {
        $this->attributes['published'] = $input ? $input : null;
    }

    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }
}
