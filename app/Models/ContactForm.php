<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactForm extends Model
{
     use SoftDeletes;

    protected $fillable = ['name', 'email', 'phone', 'position', 'company', 'comments', 'origen', 'read', 'user_id'];
    protected $hidden = [];
    
    public static function boot()
    {
        parent::boot();
        ContactForm::observe(new \App\Observers\ContactFormObserver);
    }

    public function setReadAttribute($input)
    {
        $this->attributes['read'] = $input ? $input : null;
    }
}
