<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class InfoPost extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $fillable = [
        'title',
        'subtitle',
        'slug',
        'excerpt',
        'content',
        'pdf',
        'url',
        'youtube',
        'published',
        'featured',
        'category_id'
    ];
    
    protected $hidden = [];
    
    public static function boot()
    {
        parent::boot();

        NewsPost::observe(new \App\Observers\SetSlugObserver);
    }

    public function setCategoryIdAttribute($input)
    {
        $this->attributes['category_id'] = $input ? $input : null;
    }

    public function setPublishedAttribute($input)
    {
        $this->attributes['published'] = $input ? $input : null;
    }
  
    public function category()
    {
        return $this->belongsTo(NewsCategory::class, 'category_id');
    }

    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(150)->height(150);
    }
    
    public function getPhotoAttribute()
    {
        $file = $this->getMedia('photo')->last();
        if ($file) {
            $file->url = $file->getUrl();
        }

        return $file;
    }

    public function getGalleryAttribute()
    {
        $files = $this->getMedia('gallery');

        $files->each(function ($item) {
            $item->url = $item->getUrl();
        });

        return $files;
    }

    public function getdatasheetAttribute()
    {
        return $this->getMedia('datasheet')->last();
    }
}
