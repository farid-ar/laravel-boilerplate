<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Slideshow extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $fillable = ['title', 'section', 'legend', 'image_desktop', 'image_mobile', 'link', 'target', 'order', 'published'];
    protected $hidden = [];
    
    

    public static $enum_target = ["_self" => "_self", "_blank" => "_blank"];

    public function setOrderAttribute($input)
    {
        $this->attributes['order'] = $input ? $input : null;
    }

    public function setPublishedAttribute($input)
    {
        $this->attributes['published'] = $input ? $input : null;
    }

    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(150)->height(150);
    }

    public function getImageDesktopAttribute()
    {
        $file = $this->getMedia('image_desktop')->last();
        if ($file) {
            $file->url = $file->getUrl();
        }
        return $file;
    }
    public function getImageMobileAttribute()
    {
        $file = $this->getMedia('image_mobile')->last();
        if ($file) {
            $file->url = $file->getUrl();
        }
        return $file;
    }
}
